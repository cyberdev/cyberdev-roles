# == Class: role::webserver
#
# Webserver role
#
# === Parameters
#
# None
#
# === Variables
#
# None
#
# === Examples
#
#  include role::webserver
#
# === Authors
#
# Rob Nelson <rnelson0@gmail.com>
#
# === Copyright
#
# Copyright 2014 Rob Nelson
#
class roles::rocksensor {

  include profiles::base  # All roles should have the base profile
  #include profiles::bro
  include profiles::suricata
  #include profiles::netsniffng
  #include profiles::elasticsearch
  include profiles::kibana
}
